using System;
using System.Collections.Generic;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using System.Linq;
using ComEd.Innovation.PJM;
using ComEd.Innovation.SFTP;
using Microsoft.Extensions.Configuration;
using System.IO;
using ComEd.Innovation.Email;

namespace ComEd.Innovation.PJMHourlyPriceFeed
{
    public static class GetScaledHourlyLMP
    {
               


        [FunctionName("GetScaledHourlyLMP")]
        public static void Run([TimerTrigger("0 0 12 * * *")]TimerInfo myTimer, ILogger log)
        {
            log.LogInformation($"C# Timer trigger function executed at: {DateTime.Now}");            

            var startDate = DateTime.Now.AddDays(-5).Date;
            DateTime priceFileDate;
           
            if ( DateTime.Now.DayOfWeek != DayOfWeek.Saturday && DateTime.Now.DayOfWeek != DayOfWeek.Sunday)
            {
               
                var pjmURL = Environment.GetEnvironmentVariable("PJMURL", EnvironmentVariableTarget.Process);
                
                var pjmPnode = Environment.GetEnvironmentVariable("PJMPNODE", EnvironmentVariableTarget.Process);
                var pjmAPIKey = Environment.GetEnvironmentVariable("PJMAPIKEY", EnvironmentVariableTarget.Process);
                var pjmPriceScalingFactor = Environment.GetEnvironmentVariable("PJMPRICESCALINGFACTOR", EnvironmentVariableTarget.Process);

                var sftpHost = Environment.GetEnvironmentVariable("SFTPHOST", EnvironmentVariableTarget.Process);
                var sftpUsername = Environment.GetEnvironmentVariable("SFTPUSERNAME", EnvironmentVariableTarget.Process);
                string sftpKeyName = Environment.GetEnvironmentVariable("SFTPKEYNAME", EnvironmentVariableTarget.Process);
                var sftpFolder = Environment.GetEnvironmentVariable("SFTPFOLDER", EnvironmentVariableTarget.Process);
                var remoteFilename = Environment.GetEnvironmentVariable("REMOTEFILENAME", EnvironmentVariableTarget.Process);
                var adminEmail = Environment.GetEnvironmentVariable("ADMINEMAIL", EnvironmentVariableTarget.Process);
                var emailSubject = Environment.GetEnvironmentVariable("EMAILSUBJECT", EnvironmentVariableTarget.Process);
                var emailMessage = Environment.GetEnvironmentVariable("EMAILMESSAGE", EnvironmentVariableTarget.Process);

                var comedRateAdder = Environment.GetEnvironmentVariable("COMEDRATEADDER", EnvironmentVariableTarget.Process);

                string baseURL = Environment.GetEnvironmentVariable("BaseURL", EnvironmentVariableTarget.Process);

                log.LogInformation("Starting to retrieve PJM Hourly LMP ");
                var pjmInterface = new PJMAPIInterface(pjmURL, pjmPnode, pjmAPIKey);

                var exportCSV = new List<string>();
                var header = "UTCDate,EPTDate,PJMScaledPrice,PJMPrice";
                string fileName;

                AzureKeyVaultRetrieve azureKeyVaultRetrieve = new AzureKeyVaultRetrieve(baseURL);
                var sftpKey = azureKeyVaultRetrieve.decodePrivateKey(azureKeyVaultRetrieve.GetSecret(sftpKeyName));

                for (int i = 0; i < 5; i++)
                {
                    priceFileDate = startDate.AddDays(i);
                    var listpjmhourlyprice = pjmInterface.getHourlyLMP(priceFileDate, double.Parse(pjmPriceScalingFactor), double.Parse(comedRateAdder));

                    log.LogInformation("Completed retrieving PJM Hourly LMP for {0}", priceFileDate.ToShortDateString());

                     exportCSV = new List<string>();
                    

                    exportCSV.Add(header);

                    exportCSV.AddRange(listpjmhourlyprice.Select(x => x.datetime_beginning_utc + "," + x.datetime_beginning_ept
                                    + "," + x.scaled_total_lmp_rt + "," + x.total_lmp_rt).ToList());

                     fileName = remoteFilename + "_" + priceFileDate.ToString("MM-dd-yyyy") + ".csv";

                    log.LogInformation($"Starting to export data to SFTPHost: {sftpHost}");

                    try
                    {
                        if (exportCSV.Count > 1) {
                            using (MemoryStream privateKeyStream = new MemoryStream())
                            {
                                var writer = new StreamWriter(privateKeyStream);

                                writer.Write(sftpKey);
                                writer.Flush();
                                privateKeyStream.Position = 0;

                                SFTPInterface SFTP = new SFTPInterface(sftpHost, sftpUsername, privateKeyStream);
                                SFTP.SendCSV(exportCSV, sftpFolder, fileName, true);
                            }
                        }
                        else
                        {                           
                            //The PJM API returned no date for the given day
                            Mail.SendMail(adminEmail, emailSubject, string.Format(emailMessage, priceFileDate.ToShortDateString()));
                        }
                        

                        log.LogInformation($"Completed export of data to SFTPHost: {sftpHost}");

                    }
                    catch (ArgumentNullException ex)
                    {
                        log.LogError("The base URL cannot be null.");
                        log.LogError(ex.ToString());
                    }

                }




            }


        }
    }
}
