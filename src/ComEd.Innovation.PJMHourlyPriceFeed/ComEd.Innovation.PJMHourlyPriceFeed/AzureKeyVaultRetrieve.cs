﻿
using System;
using System.Text;
using System.Threading.Tasks;
using Azure;
using Azure.Identity;
using Azure.Security.KeyVault.Secrets;
using Serilog;

namespace ComEd.Innovation.PJMHourlyPriceFeed
{
    public class AzureKeyVaultRetrieve
    {
        private string BASE_URL;

        public AzureKeyVaultRetrieve(string baseURL)
        {
            if (baseURL == null || baseURL == "")
                throw new ArgumentNullException("Error retrieving Key Vault URL");

            BASE_URL = baseURL;
        }


        public string GetSecret(string keyVaultName)
        {
            try
            {
                var client = new SecretClient(new Uri(BASE_URL), new DefaultAzureCredential());
                var secret = Task.Run(
                      () => client.GetSecretAsync(keyVaultName)).ConfigureAwait(false).GetAwaiter().GetResult();

                return secret.Value.Value;
            }
            catch (AggregateException ex)
            {
                Log.Error("There is an issue with base url.");
                Log.Error(ex.ToString());

                return null;
            }
            catch (RequestFailedException ex)
            {
                Log.Error("The key vault secret was not found. Please check the name of your secret name.");
                Log.Error(ex.ToString());

                return null;
            }
        }

        public string decodePrivateKey(string key)
        {
            //Decode privateKey string from Base64 when stored in Azure KeyVault
            key ??= "";
            byte[] data = Convert.FromBase64String(key);
            return Encoding.UTF8.GetString(data);
        }
    }
}

