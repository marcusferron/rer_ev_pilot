﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;

namespace ComEd.Innovation.SFTP
{
    public class SFTPInterface
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string Host;
        private string Username;
        private List<AuthenticationMethod> AuthenticationMethods;


        public SFTPInterface(string host, string username, string password, string privateKeyFile)
        {

            try
            {
                Host = host;
                Username = username;
                var keyFile = new PrivateKeyFile(privateKeyFile);
                var keyFiles = new[] { keyFile };

                AuthenticationMethods = new List<AuthenticationMethod>();
                AuthenticationMethods.Add(new PasswordAuthenticationMethod(username, password));
                AuthenticationMethods.Add(new PrivateKeyAuthenticationMethod(username, keyFiles));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }

        }

        public SFTPInterface(string host, string username, Stream privateKeyFile)
        {

            try
            {
                Host = host;
                Username = username;
                var keyFile = new PrivateKeyFile(privateKeyFile);
                var keyFiles = new[] { keyFile };

                AuthenticationMethods = new List<AuthenticationMethod>();
               // AuthenticationMethods.Add(new PasswordAuthenticationMethod(username, password));
                AuthenticationMethods.Add(new PrivateKeyAuthenticationMethod(username, keyFiles));
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }

        }

        public SFTPInterface(string host, string username, string password)
        {
            Host = host;
            Username = username;

            AuthenticationMethods = new List<AuthenticationMethod>();
            AuthenticationMethods.Add(new PasswordAuthenticationMethod(username, password));


        }
        public void SendFile(string localPath, string remotePath, string remoteFile, bool shouldOverride = true)
        {
            var con = new ConnectionInfo(Host, Username, AuthenticationMethods.ToArray());
            using (var sftpClient = new SftpClient(con))
            {
                sftpClient.Connect();
                sftpClient.ChangeDirectory(remotePath);

                using (var str = System.IO.File.OpenRead(localPath))
                {
                    sftpClient.UploadFile(str, remoteFile, shouldOverride);
                }
                sftpClient.Disconnect();
            }

        }

        public void SendCSV(List<string> csvFile, string remotePath, string remoteFile, bool shouldOverride = true)
        {
            var con = new ConnectionInfo(Host, Username, AuthenticationMethods.ToArray());           

            using (var sftpClient = new SftpClient(con))
            {
                sftpClient.Connect();
                sftpClient.ChangeDirectory(remotePath);

                using (MemoryStream stringInMemoryStream = new MemoryStream())
                {
                    var sw = new StreamWriter(stringInMemoryStream);
                    try
                    {
                        foreach (var message in csvFile)
                        { 
                            sw.WriteLine(message);
                        }
                        sw.Flush();
                        stringInMemoryStream.Seek(0, SeekOrigin.Begin);

                        sftpClient.UploadFile(stringInMemoryStream, remoteFile, shouldOverride);
                    }
                    finally
                    {
                        sw.Dispose();
                    }
                }
                    
                sftpClient.Disconnect();
            }

        }
        public void GetFile(string localPath, string remotePath, string remoteFile, bool shouldOverride = true)
        {
            var con = new ConnectionInfo(Host, Username, AuthenticationMethods.ToArray());
            using (var sftpClient = new SftpClient(con))
            {
                sftpClient.Connect();
                sftpClient.ChangeDirectory(remotePath);

                using (Stream fileStream = File.Create(localPath))
                {
                    sftpClient.DownloadFile(remotePath + "/" + remoteFile, fileStream);
                    sftpClient.Disconnect();
                }

            }

        }
        public List<string> GetFilesInFolder(string remotefolder, string localfolder, bool deleteFromSFTP = false)
        {
            List<string> fileList = new List<string>();

            var con = new ConnectionInfo(Host, Username, AuthenticationMethods.ToArray());
            using (var sftpClient = new SftpClient(con))
            {
                sftpClient.Connect();
                var files = sftpClient.ListDirectory(remotefolder);

                foreach (var file in files)
                {
                    string remoteFileName = file.Name;
                    if (!(file.Name == "..") && !(file.Name == "."))
                    {

                        using (Stream file1 = File.OpenWrite(localfolder + remoteFileName))
                        {
                            sftpClient.DownloadFile(remotefolder + remoteFileName, file1);

                            if (deleteFromSFTP)
                            {
                                try
                                {
                                    sftpClient.DeleteFile(file.FullName);
                                }
                                catch (Exception ex)
                                {
                                    log.Error("Could not delete file: " + file + " from SFTP server." + ex.ToString());
                                }
                            }
                            fileList.Add(localfolder + file);
                        }
                    }
                }

                return fileList;
            }
        }

        public void DeleteFile(string remotepath, string remoteFile)
        {
            var con = new ConnectionInfo(Host, Username, AuthenticationMethods.ToArray());
            using (var sftpClient = new SftpClient(con))
            {
                sftpClient.DeleteFile(remotepath + "/" + remoteFile);
            }

        }

    }
}
