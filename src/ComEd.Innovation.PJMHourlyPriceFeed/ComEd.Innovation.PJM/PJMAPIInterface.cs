﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace ComEd.Innovation.PJM
{
    public class PJMAPIInterface
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType); 
        
        private string _pnode;

        private string _pjmapiKey;

        private string _pjmUrl;

        private int _rowCount = 24; // Max number of 5 minute intervals in a day
        private int startRow = 1;

        private readonly string RT_HRL_LMPS = "api/v1/rt_hrl_lmps";
        private readonly string PJM_HRL_DATA_OBJECT = "items";
        private readonly string PJM_HRL_DATE_FIELD = "datetime_beginning_utc";

        private HttpClient client;

        public PJMAPIInterface(String pjmUrl, string pnode, string pjmapiKey)
        {
            _pnode = pnode;

            _pjmapiKey = pjmapiKey;

            _pjmUrl = pjmUrl;            

            HttpClientHandler handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };

            client = new HttpClient(handler);

            client.BaseAddress = new Uri(_pjmUrl);

            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", _pjmapiKey);
        }

        public List<PJMHourlyPrice> getHourlyLMP(DateTime hourlyLMPDate, double pjmScalingFactor, double comedRateAdder )
        {
            List<PJMHourlyPrice> pjmHourlyPrice = null;
            

            try
            {

                var pjmhrl_lmps = String.Format(RT_HRL_LMPS + "?rowCount={0}&startRow={1}&datetime_beginning_ept={2}&pnode_id={3}", 
                    _rowCount, startRow, hourlyLMPDate, _pnode);

                var response = client.GetAsync(pjmhrl_lmps).Result;


                string jsonResult = "";

                if (response.IsSuccessStatusCode)
                {
                    jsonResult = response.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    //Add logging
                    log.Error("Please check the user credenital as the following response code :  " + response.StatusCode.ToString() + " has occurred.");
                }

                if (jsonResult != "")
                {
                    JObject pjmJObject = JObject.Parse(jsonResult);

                    pjmHourlyPrice = (from f in pjmJObject[PJM_HRL_DATA_OBJECT]
                                      orderby f[PJM_HRL_DATE_FIELD] ascending
                                      select new PJMHourlyPrice
                                      {
                                          datetime_beginning_utc = (string)f["datetime_beginning_utc"],
                                          datetime_beginning_ept = (string)f["datetime_beginning_ept"],
                                          pnode_id = (string)f["pnode_id"],
                                          pnode_name = (string)f["pnode_name"],
                                          voltage = (string)f["voltage"],
                                          equipment = (string)f["equipment"],
                                          type = (string)f["type"],
                                          zone = (string)f["zone"],
                                          system_energy_price_rt = (string)f["system_energy_price_rt"],
                                          total_lmp_rt = (string)f["total_lmp_rt"],
                                          congestion_price_rt = (string)f["congestion_price_rt"],
                                          marginal_loss_price_rt = (string)f["marginal_loss_price_rt"],
                                          row_is_current = (string)f["row_is_current"],
                                          version_nbr = (string)f["version_nbr"]
                                      }).ToList();

                    pjmHourlyPrice = pjmHourlyPrice.Select(p => { p.scaled_total_lmp_rt = System.Math.Round(((double.Parse(p.total_lmp_rt) * pjmScalingFactor) + comedRateAdder), 5).ToString(); return p; }).ToList();

                }
            }
            catch (WebException e)
            {
                log.Error("An exception occurred when getting the latest Hourly lmp from PJM. Exception: " + e.ToString(), e);

                throw new ApplicationException(e.Message, e);
            }
            catch (AggregateException e)
            {
                log.Error("An exception occurred when getting the latest hourly lmp from PJM. Please check your URL.  Exception: " + e.ToString(), e);

                throw new ApplicationException(e.Message, e);
            }            

            return pjmHourlyPrice;
        }
    }
}
