﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComEd.Innovation.PJM
{
    public class PJMHourlyPrice
    {
        public string datetime_beginning_utc;
        public string datetime_beginning_ept;
        public string pnode_id;
        public string pnode_name;
        public string voltage;
        public string equipment;
        public string type;
        public string zone;
        public string system_energy_price_rt;
        public string total_lmp_rt;
        public string scaled_total_lmp_rt;
        public string congestion_price_rt;
        public string marginal_loss_price_rt;
        public string row_is_current;
        public string version_nbr;
    }
}
