﻿using System;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace ComEd.Innovation.Email
{
    public class Mail
    {


        /// <summary>
        /// 
        /// </summary>
        private const string CONFIG_SMTPSERVER = "SMTPSERVERADDRESS";

        /// <summary>
        /// 
        /// </summary>
        private const string CONFIG_SMTPUSERNAME = "SMTPUSERNAME";

        /// <summary>
        /// 
        /// </summary>
        private const string CONFIG_SMTPPASSWORD = "SMTPPASSWORD";

        /// <summary>
        /// FROMEMAIL -- indicates the config file 
        /// </summary>
        private const string CONFIG_FROMEMAIL = "FROMEMAIL";

        /// <summary>
        /// 
        /// </summary>
        private const string CONFIG_SMTPPROT = "SMTPPROT";

        /// <summary>
        /// 
        /// </summary>
        private const string CONFIG_SMTPUSESSL = "SMTPUSESSL";


        /// <summary>
        /// 
        /// </summary>
        private const string CONFIG_EMAILDELIMITER = "EMAILDELIMITER";

        /// <summary>
        /// 
        /// </summary>
        private const string DEFAULTEMAILDELIMITER = ";";


        #region Sending Mail

        /// <summary>
        /// Sends mail using the smtpClient. Exception is captured and writted to the event log.
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="mailMsg"></param>
        private static bool SendMail(SmtpClient mail, MailMessage mailMsg)
        {
            try
            {
                mail.Send(mailMsg);
                mailMsg.Dispose();
                return true;
            }
            catch
            {
                try
                {
                    //EventLog.WriteEntry("itec-energy usage data", "Mail sending error. Please check the mail id and smtp", EventLogEntryType.Error);
                    // Reaplce with Log4net loggin machanisam...
                }
                catch { }
                return false;
            }
        }

        public static bool SendMail(string toAddress, string subject, string message)
        {
            return SendMail(toAddress, "", "", "", subject, message);
        }
        public static bool SendMail(string toAddress, string subject, string message, bool isBodyHtml)
        {
            string[] attachedFiles = null;
            return SendMail(toAddress, "", "", "", subject, message, attachedFiles, isBodyHtml);
        }
        public static bool SendMail(string toAddress, string subject, string message, string[] attachFilePaths)
        {
            string _fromAddress = Environment.GetEnvironmentVariable(CONFIG_FROMEMAIL, EnvironmentVariableTarget.Process); 

            if (string.IsNullOrEmpty(_fromAddress)) return false;

            return SendMail(toAddress, "", "", _fromAddress, subject, message, attachFilePaths);
        }

        /// <summary>
        /// Sends mail using the provided parameters. 
        /// </summary>
        public static bool SendMail(string toAddress, string bccAddress, string ccAddress, string fromAddress, string subject, string message)
        {
            string[] attachments = { };
            string _fromAddress = Environment.GetEnvironmentVariable(CONFIG_FROMEMAIL, EnvironmentVariableTarget.Process);
            if (!string.IsNullOrEmpty(fromAddress))
                _fromAddress = fromAddress;

            return SendMail(toAddress, bccAddress, ccAddress, _fromAddress, subject, message, attachments);
        }

        /// <summary>
        /// Sends mail using the provided parameters. 
        /// </summary>
        public static bool SendMail(string toAddress, string bccAddress, string ccAddress, string fromAddress, string displayname, string subject, string message)
        {
            string[] attachments = { };
            string _fromAddress =  Environment.GetEnvironmentVariable(CONFIG_FROMEMAIL, EnvironmentVariableTarget.Process);
            if (!string.IsNullOrEmpty(fromAddress))
                _fromAddress = fromAddress;

            return SendMail(toAddress, bccAddress, ccAddress, _fromAddress, displayname, subject, message, attachments);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="bccAddress"></param>
        /// <param name="ccAddress"></param>
        /// <param name="fromAddress"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <param name="attachFilepaths"></param>
        /// <returns></returns>
        public static bool SendMail(string toAddress, string bccAddress, string ccAddress, string fromAddress, string subject, string message, string[] attachFilepaths)
        {
            return SendMail(toAddress, bccAddress, ccAddress, fromAddress, subject, message, attachFilepaths, false);
        }

        // <summary>
        /// 
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="bccAddress"></param>
        /// <param name="ccAddress"></param>
        /// <param name="fromAddress"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <param name="attachFilepaths"></param>
        /// <returns></returns>
        public static bool SendMail(string toAddress, string bccAddress, string ccAddress, string fromAddress, string displayname, string subject, string message, string[] attachFilepaths)
        {
            return SendMail(toAddress, bccAddress, ccAddress, fromAddress, displayname, subject, message, attachFilepaths, false);
        }
        /// <summary>
        /// Send mail using the provided parameters. Creates the smtpclient object and message object.
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="bccAddress"></param>
        /// <param name="ccAddress"></param>
        /// <param name="fromAddress"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <param name="attachFilepaths"></param>
        /// <param name="isBodyHtml"></param>
        /// <exception cref="ITECException">Invalid inputs</exception>
        public static bool SendMail(string toAddress, string bccAddress, string ccAddress, string fromAddress, string subject, string message, string[] attachFilepaths, bool isBodyHtml)
        {
            if (string.IsNullOrEmpty(toAddress) || string.IsNullOrEmpty(message))
                return false;

            try
            {
                string smtpHost = Environment.GetEnvironmentVariable(CONFIG_SMTPSERVER, EnvironmentVariableTarget.Process);
                string userName =  Environment.GetEnvironmentVariable(CONFIG_SMTPUSERNAME, EnvironmentVariableTarget.Process);
                string password =  Environment.GetEnvironmentVariable(CONFIG_SMTPPASSWORD, EnvironmentVariableTarget.Process);
                string frmAddress =  Environment.GetEnvironmentVariable(CONFIG_FROMEMAIL, EnvironmentVariableTarget.Process);

                if (!string.IsNullOrEmpty(fromAddress))
                    frmAddress = fromAddress;

                if (string.IsNullOrEmpty(smtpHost)) return false;

                SmtpClient smtp = new SmtpClient(smtpHost);
                smtp.Credentials = new NetworkCredential(userName, password);
                MailMessage mailMsg = new MailMessage();
                mailMsg.BodyEncoding = Encoding.Default;

                string smtpPort =  Environment.GetEnvironmentVariable(CONFIG_SMTPPROT, EnvironmentVariableTarget.Process);
                if (!string.IsNullOrEmpty(smtpPort))
                {
                    smtp.Port = Convert.ToInt32(smtpPort);
                }
                string smptUseSSL =  Environment.GetEnvironmentVariable(CONFIG_SMTPUSESSL, EnvironmentVariableTarget.Process);
                if (!string.IsNullOrEmpty(smptUseSSL))
                {
                    smtp.EnableSsl = Convert.ToBoolean(smptUseSSL);
                }

                string delimiterList =  Environment.GetEnvironmentVariable(CONFIG_EMAILDELIMITER, EnvironmentVariableTarget.Process);

                string[] delimiter = { delimiterList };

                foreach (string address in toAddress.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
                    mailMsg.To.Add(new MailAddress(address));

                if (bccAddress != null && bccAddress.Length > 0)
                {
                    foreach (string address in bccAddress.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
                        mailMsg.Bcc.Add(new MailAddress(address));
                }
                if (ccAddress != null && ccAddress.Length > 0)
                {
                    foreach (string address in ccAddress.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
                        mailMsg.CC.Add(new MailAddress(address));
                }
                if (attachFilepaths != null && attachFilepaths.Length > 0)
                {
                    foreach (string filePath in attachFilepaths)
                        mailMsg.Attachments.Add(new Attachment(filePath));
                }
                mailMsg.BodyEncoding = Encoding.ASCII;
                mailMsg.From = new MailAddress(frmAddress);
                mailMsg.Subject = subject;
                mailMsg.IsBodyHtml = isBodyHtml;
                mailMsg.Body = message;
                return SendMail(smtp, mailMsg);

            }
            catch (Exception ex)
            {
                //throw ex;


            }
            return false;
        }

        /// <summary>
        /// Send mail using the provided parameters. Creates the smtpclient object and message object.
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="bccAddress"></param>
        /// <param name="ccAddress"></param>
        /// <param name="fromAddress"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <param name="attachFilepaths"></param>
        /// <param name="isBodyHtml"></param>
        /// <exception cref="ITECException">Invalid inputs</exception>
        public static bool SendMail(string toAddress, string bccAddress, string ccAddress, string fromAddress, string displayname, string subject, string message, string[] attachFilepaths, bool isBodyHtml)
        {
            if (string.IsNullOrEmpty(toAddress) || string.IsNullOrEmpty(message))
                return false;

            try
            {
                string smtpHost =  Environment.GetEnvironmentVariable(CONFIG_SMTPSERVER, EnvironmentVariableTarget.Process);
                string userName =  Environment.GetEnvironmentVariable(CONFIG_SMTPUSERNAME, EnvironmentVariableTarget.Process);
                string password =  Environment.GetEnvironmentVariable(CONFIG_SMTPPASSWORD, EnvironmentVariableTarget.Process);
                string frmAddress =  Environment.GetEnvironmentVariable(CONFIG_FROMEMAIL, EnvironmentVariableTarget.Process);

                if (!string.IsNullOrEmpty(fromAddress))
                    frmAddress = fromAddress;

                if (string.IsNullOrEmpty(smtpHost)) return false;

                SmtpClient smtp = new SmtpClient(smtpHost);
                smtp.Credentials = new NetworkCredential(userName, password);
                MailMessage mailMsg = new MailMessage();
                mailMsg.BodyEncoding = Encoding.Default;

                string smtpPort =  Environment.GetEnvironmentVariable(CONFIG_SMTPPROT, EnvironmentVariableTarget.Process);
                if (!string.IsNullOrEmpty(smtpPort))
                {
                    smtp.Port = Convert.ToInt32(smtpPort);
                }
                string smptUseSSL =  Environment.GetEnvironmentVariable(CONFIG_SMTPUSESSL, EnvironmentVariableTarget.Process);
                if (!string.IsNullOrEmpty(smptUseSSL))
                {
                    smtp.EnableSsl = Convert.ToBoolean(smptUseSSL);
                }

                string delimiterList =  Environment.GetEnvironmentVariable(CONFIG_EMAILDELIMITER, EnvironmentVariableTarget.Process);

                string[] delimiter = { delimiterList };

                foreach (string address in toAddress.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
                    mailMsg.To.Add(new MailAddress(address));

                if (bccAddress != null && bccAddress.Length > 0)
                {
                    foreach (string address in bccAddress.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
                        mailMsg.Bcc.Add(new MailAddress(address));
                }
                if (ccAddress != null && ccAddress.Length > 0)
                {
                    foreach (string address in ccAddress.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
                        mailMsg.CC.Add(new MailAddress(address));
                }
                if (attachFilepaths != null && attachFilepaths.Length > 0)
                {
                    foreach (string filePath in attachFilepaths)
                        mailMsg.Attachments.Add(new Attachment(filePath));
                }
                mailMsg.BodyEncoding = Encoding.ASCII;
                mailMsg.From = new MailAddress(frmAddress, displayname);
                mailMsg.Subject = subject;
                mailMsg.IsBodyHtml = isBodyHtml;
                mailMsg.Body = message;
                return SendMail(smtp, mailMsg);

            }
            catch (Exception ex)
            {
                //throw ex;


            }
            return false;
        }

        #endregion Sending Mail

    }
}
